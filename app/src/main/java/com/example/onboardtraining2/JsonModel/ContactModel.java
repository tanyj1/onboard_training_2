package com.example.onboardtraining2.JsonModel;

public class ContactModel {
    private String Avatar;
    private String FirstName;
    private String LastName;
    private String Email;

    public ContactModel() {


    }

    public ContactModel(String avatar, String firstName, String lastName, String email) {
        Avatar = avatar;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String avatar) {
        Avatar = avatar;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}

