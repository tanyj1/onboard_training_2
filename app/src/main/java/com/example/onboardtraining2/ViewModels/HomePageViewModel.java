package com.example.onboardtraining2.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.onboardtraining2.adapter.HomePageAdapter;
import com.example.onboardtraining2.repository.HomePageRepo;

import java.util.List;

public class HomePageViewModel extends AndroidViewModel {

    private HomePageRepo homePageRepo ;
    public HomePageViewModel(@NonNull Application application) {
        super(application);
        homePageRepo = HomePageRepo.getInstance();

    }
    public LiveData<List<HomePageAdapter.Contacts>> getMyContactsAdapterListLiveData() {
        return homePageRepo.getContactsAdapterListLiveData();
    }

    public void loadMyContactsAdapterList() {
        homePageRepo.loadContactsAdapterList(getApplication());
    }

    public void addContact(String FirstName, String LastName, String Email,String Avatar) {
        homePageRepo.addContact( getApplication(), FirstName, LastName,Email,Avatar);
    }

    public void deleteContacts(Long ContactId) {
        homePageRepo.deleteContactDetails( getApplication(), ContactId);
    }

    public HomePageAdapter.Contacts getContactDetails(Long ContactId) {
        return homePageRepo.getContactDetails(getApplication(),ContactId);
    }

    public void editContact(Long ContactId,String Name,String ContactNumber,String Email) {
        homePageRepo.editContactDetails( getApplication(), ContactId,Name,ContactNumber,Email);
    }



}
