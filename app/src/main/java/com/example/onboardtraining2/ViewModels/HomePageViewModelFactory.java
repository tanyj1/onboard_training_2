package com.example.onboardtraining2.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class HomePageViewModelFactory implements ViewModelProvider.Factory{
    private Application Application;

    public HomePageViewModelFactory(Application application) {
        Application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomePageViewModel(Application);
    }

}
