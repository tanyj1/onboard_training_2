package com.example.onboardtraining2.database.entities;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "CONTACT".
*/
public class ContactDao extends AbstractDao<Contact, Long> {

    public static final String TABLENAME = "CONTACT";

    /**
     * Properties of entity Contact.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "Id", true, "_id");
        public final static Property FirstName = new Property(1, String.class, "FirstName", false, "FIRST_NAME");
        public final static Property LastName = new Property(2, String.class, "LastName", false, "LAST_NAME");
        public final static Property Email = new Property(3, String.class, "Email", false, "EMAIL");
        public final static Property Avatar = new Property(4, String.class, "Avatar", false, "AVATAR");
    }


    public ContactDao(DaoConfig config) {
        super(config);
    }
    
    public ContactDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"CONTACT\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: Id
                "\"FIRST_NAME\" TEXT," + // 1: FirstName
                "\"LAST_NAME\" TEXT," + // 2: LastName
                "\"EMAIL\" TEXT," + // 3: Email
                "\"AVATAR\" TEXT);"); // 4: Avatar
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"CONTACT\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Contact entity) {
        stmt.clearBindings();
 
        Long Id = entity.getId();
        if (Id != null) {
            stmt.bindLong(1, Id);
        }
 
        String FirstName = entity.getFirstName();
        if (FirstName != null) {
            stmt.bindString(2, FirstName);
        }
 
        String LastName = entity.getLastName();
        if (LastName != null) {
            stmt.bindString(3, LastName);
        }
 
        String Email = entity.getEmail();
        if (Email != null) {
            stmt.bindString(4, Email);
        }
 
        String Avatar = entity.getAvatar();
        if (Avatar != null) {
            stmt.bindString(5, Avatar);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Contact entity) {
        stmt.clearBindings();
 
        Long Id = entity.getId();
        if (Id != null) {
            stmt.bindLong(1, Id);
        }
 
        String FirstName = entity.getFirstName();
        if (FirstName != null) {
            stmt.bindString(2, FirstName);
        }
 
        String LastName = entity.getLastName();
        if (LastName != null) {
            stmt.bindString(3, LastName);
        }
 
        String Email = entity.getEmail();
        if (Email != null) {
            stmt.bindString(4, Email);
        }
 
        String Avatar = entity.getAvatar();
        if (Avatar != null) {
            stmt.bindString(5, Avatar);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Contact readEntity(Cursor cursor, int offset) {
        Contact entity = new Contact( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // Id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // FirstName
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // LastName
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // Email
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4) // Avatar
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Contact entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setFirstName(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setLastName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setEmail(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setAvatar(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Contact entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Contact entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(Contact entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
