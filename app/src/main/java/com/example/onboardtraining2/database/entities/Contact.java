package com.example.onboardtraining2.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Contact {

    @org.greenrobot.greendao.annotation.Id(autoincrement = true)
    private Long Id; // For keeping things simple lets just say its an id
    private String FirstName;
    private String LastName;
    private String Email;
    private String Avatar;

    @Generated(hash = 1274014413)
    public Contact(Long Id, String FirstName, String LastName, String Email,
                   String Avatar) {
        this.Id = Id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Email = Email;
        this.Avatar = Avatar;
    }

    @Generated(hash = 672515148)
    public Contact() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getFirstName() {
        return this.FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return this.LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getEmail() {
        return this.Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getAvatar() {
        return this.Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }

}
