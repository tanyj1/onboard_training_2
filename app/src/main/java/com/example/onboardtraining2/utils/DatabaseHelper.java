package com.example.onboardtraining2.utils;

import android.content.Context;

import com.example.onboardtraining2.database.DbOpenHelper;
import com.example.onboardtraining2.database.entities.DaoMaster;
import com.example.onboardtraining2.database.entities.DaoSession;

public class DatabaseHelper {
    private static DaoSession mInstance;

    public static DaoSession getInstance(Context context) {
        if (mInstance == null) {
            synchronized (DatabaseHelper.class) {
                if (mInstance == null) {
                    DbOpenHelper dbOpenHelper = new DbOpenHelper(context, "app");

                    mInstance = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
                }
            }
        }

        return mInstance;
    }

    private DatabaseHelper() {
    }


}
