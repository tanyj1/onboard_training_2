package com.example.onboardtraining2.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.onboardtraining2.BuildConfig;
import com.example.onboardtraining2.JsonModel.MainModel;
import com.example.onboardtraining2.R;
import com.example.onboardtraining2.ViewModels.MainViewModel;
import com.example.onboardtraining2.activity.HomePageActivity;
import com.example.onboardtraining2.activity.TermsActivity;
import com.example.onboardtraining2.utils.NetworkHelper;
import com.example.onboardtraining2.utils.SharedPreferenceHelper;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class MainFragment extends Fragment {

    @BindView(R.id.UserNameInput)
    protected EditText mEditTextUserNameInput;
    @BindView(R.id.PasswordInput)
    protected EditText mEditPasswordInput;
    @BindView(R.id.LoginButton)
    protected Button LoginButton;
    @BindView(R.id.TermTextView)
    protected TextView TermTextView;
    private MainViewModel mViewModel;
    private Unbinder mUnbinder;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        // TODO: Use the ViewModel
        mEditTextUserNameInput.setText("eve.holt@reqres.in");
        mEditPasswordInput.setText("cityslicka");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @OnClick(R.id.LoginButton)
    protected void doLogin() {

        LoginButton.setEnabled(false);


        LoginButton.setText(R.string.loading_message);
        final String email = mEditTextUserNameInput.getText().toString().trim();
        final String password = mEditPasswordInput.getText().toString();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content(R.string.blank_message)
                    .positiveText("OK")
                    .show();
            LoginButton.setEnabled(true);
            LoginButton.setText(R.string.login_message);
            return;


        }

        if (isNetworkAvailable()) {


            // Call login API.
            String url = BuildConfig.BASE_API_URL + "login";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();
                    // Convert JSON string to Java object.
                    MainModel responseModel = new GsonBuilder().create().fromJson(response, MainModel.class);

                    // Save user's email to shared preference.
                    SharedPreferenceHelper.getInstance(getActivity())
                            .edit()
                            .putString("email", email)
                            .putString("token", responseModel.token)
                            .apply();

                    startActivity(new Intent(getActivity(), HomePageActivity.class));
                    getActivity().finish();
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Timber.e("d;;Login error: %s", error.getMessage());
                    // Show error in popup dialog.
                    new MaterialDialog.Builder(getActivity())
                            .title(R.string.try_again_message)
                            .content(R.string.invalid_login_messgae)
                            .positiveText(R.string.ok_message)
                            .show();

                    LoginButton.setEnabled(true);
                    LoginButton.setText(R.string.login_message);
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", email);
                    params.put("password", password);

                    return params;
                }
            };

            NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
        } else {
            new MaterialDialog.Builder(getActivity())
                    .title(R.string.no_internet_connection)
                    .content(R.string.no_internet_connection_content)
                    .positiveText(R.string.ok_message)
                    .show();

            LoginButton.setEnabled(true);
            LoginButton.setText(R.string.login_message);
        }


    }


    @OnClick(R.id.TermTextView)
    protected void showTerm() {
        startActivity(new Intent(getActivity(), TermsActivity.class));


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
