package com.example.onboardtraining2.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.onboardtraining2.R;
import com.example.onboardtraining2.ViewModels.TermViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TermFragment extends Fragment {

    private TermViewModel mViewModel;
    private Unbinder mUnbinder;
    @BindView(R.id.termWebView)
    protected WebView termWebView;

    public static TermFragment newInstance() {
        return new TermFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.term_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        termWebView.setWebViewClient(new WebViewClient());
        termWebView.loadUrl("https://www.termsfeed.com/blog/sample-terms-and-conditions-template/");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

}
