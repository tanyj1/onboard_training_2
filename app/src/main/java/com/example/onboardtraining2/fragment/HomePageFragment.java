package com.example.onboardtraining2.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.onboardtraining2.R;
import com.example.onboardtraining2.ViewModels.HomePageViewModel;
import com.example.onboardtraining2.ViewModels.HomePageViewModelFactory;
import com.example.onboardtraining2.activity.ContactDetailsActivity;
import com.example.onboardtraining2.activity.MainActivity;
import com.example.onboardtraining2.adapter.HomePageAdapter;
import com.example.onboardtraining2.database.entities.Contact;
import com.example.onboardtraining2.utils.SharedPreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomePageFragment extends Fragment implements HomePageAdapter.Callbacks {
    private static final String TAG = ContactDetailsActivity.class.getSimpleName();
    private HomePageViewModel HomePageViewModel;
    private Unbinder mUnbinder;
    private HomePageAdapter Adapter;
    private final int REQUEST_CODE_MY_CONTACT_DETAIL = 300;

    @BindView(R.id.HomePageRecyclerView)
    protected RecyclerView HomePageRecyclerView;

    @BindView(R.id.no_contact_image)
    protected ImageView no_contact_image;

    @BindView(R.id.no_contact_text_view)
    protected TextView no_contact_text_view;

    public static HomePageFragment newInstance() {
        return new HomePageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_page_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //mViewModel = ViewModelProviders.of(this).get(HomePageViewModel.class);
        // TODO: Use the ViewModel
        initView();
        initViewModel();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    private void initView() {
        Adapter = new HomePageAdapter(getActivity());
        Adapter.setCallbacks(this);

        HomePageRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        HomePageRecyclerView.setHasFixedSize(true);
        HomePageRecyclerView.setAdapter(Adapter);
    }

    private void initViewModel() {
        //mViewModel = ViewModelProviders.of(this).get(HomePageViewModel.class);
        HomePageViewModel = new ViewModelProvider(this, new HomePageViewModelFactory(getActivity().getApplication())).get(HomePageViewModel.class);
        HomePageViewModel.getMyContactsAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<HomePageAdapter.Contacts>>() {

            @Override
            public void onChanged(List<HomePageAdapter.Contacts> dataHolders) {
                Adapter.setData(dataHolders);
                Adapter.notifyDataSetChanged();

                if (dataHolders.isEmpty()) {
                    HomePageRecyclerView.setVisibility(View.GONE);
                    no_contact_text_view.setVisibility(View.VISIBLE);
                    no_contact_image.setVisibility(View.VISIBLE);
                }else{
                    HomePageRecyclerView.setVisibility(View.VISIBLE);
                    no_contact_text_view.setVisibility(View.GONE);
                    no_contact_image.setVisibility(View.GONE);
                }

            }
        });


        // Load data into adapter.
        HomePageViewModel.loadMyContactsAdapterList();
    }

    @Override
    public void onListItemClicked(HomePageAdapter.Contacts data) {
        Intent intent = new Intent(getActivity(), ContactDetailsActivity.class);
        Bundle extras = new Bundle();
        extras.putLong(ContactDetailsActivity.EXTRA_LONG_MY_CONTACT_ID, data.id);
        extras.putString("ACTION", "VIEW_ACTION");
        intent.putExtras(extras);
        startActivityForResult(intent, REQUEST_CODE_MY_CONTACT_DETAIL);
    }

    @Override
    public void onDeleteButtonClicked(HomePageAdapter.Contacts data) {

        new MaterialDialog.Builder(getActivity())
                .title(R.string.delete_confirmation)
                .negativeText(R.string.no_message)
                .positiveText(R.string.yes_message)
                .onPositive((dialog, which) -> {
                    HomePageViewModel.deleteContacts(data.id);
                    Toast.makeText(getActivity(), R.string.delete_successful_message, Toast.LENGTH_SHORT).show();


                })
                .show();

    }

    @Override
    public void OnEditButtonClicked(HomePageAdapter.Contacts data) {
        Intent intent = new Intent(getActivity(), ContactDetailsActivity.class);
        Bundle extras = new Bundle();
        extras.putLong(ContactDetailsActivity.EXTRA_LONG_MY_CONTACT_ID, data.id);
        extras.putString("ACTION", "EDIT_ACTION");
        intent.putExtras(extras);
        startActivityForResult(intent, REQUEST_CODE_MY_CONTACT_DETAIL);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home_page, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_logout) {


            new MaterialDialog.Builder(getActivity())
                    .title(R.string.log_out_confitmation)
                    .negativeText(R.string.no_message)
                    .positiveText(R.string.yes_message)
                    .onPositive((dialog, which) -> {

                        // Logout user.
                        // Clear all user's data.
                        SharedPreferenceHelper.getInstance(getActivity())
                                .edit()
                                .clear()
                                .apply();

                        // Go to login page.
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();


                    })
                    .show();

        } else if (itemId == R.id.add_contact) {
            String url = "https://reqres.in/api/users?page=2";
            List<Contact> contactList = new ArrayList<>();

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Contact contact = new Contact();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String email = jsonObject.getString("email");
                            String firstName = jsonObject.getString("first_name");
                            String lastName = jsonObject.getString("last_name");
                            String avatar = jsonObject.getString("avatar");
                            HomePageViewModel = new ViewModelProvider(getActivity(), new HomePageViewModelFactory(getActivity().getApplication())).get(HomePageViewModel.class);
                            HomePageViewModel.addContact(firstName, lastName, email, avatar);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            requestQueue.add(jsonObjectRequest);


        }
        return super.onOptionsItemSelected(item);
    }
}
