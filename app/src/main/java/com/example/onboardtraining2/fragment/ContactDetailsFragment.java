package com.example.onboardtraining2.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.onboardtraining2.R;
import com.example.onboardtraining2.ViewModels.ContactDetailsViewModel;
import com.example.onboardtraining2.ViewModels.HomePageViewModel;
import com.example.onboardtraining2.ViewModels.HomePageViewModelFactory;
import com.example.onboardtraining2.activity.ContactDetailsActivity;
import com.example.onboardtraining2.adapter.HomePageAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ContactDetailsFragment extends Fragment {
    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_CONTACT_ID";
    private ContactDetailsViewModel mViewModel;
    private Unbinder mUnbinder;
    private HomePageViewModel ContactDetailsViewModel;
    private Long ContactId = 0L;
    private String action;
    private HomePageViewModel HomePageViewModel;
    private String Name, ContactNumber, Email;

    @BindView(R.id.FirstNamePlaceholder)
    protected TextView FirstNamePlaceholder;

    @BindView(R.id.ContactDetailsAvatar)
    protected ImageView ContactDetailsAvatar;

    @BindView(R.id.EmailPlaceholder)
    protected TextView EmailPlaceholder;


    @BindView(R.id.LastNamePlaceholder)
    protected TextView LastNamePlaceholder;

    public static ContactDetailsFragment newInstance(long ContactId, String action) {

        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, ContactId);
        args.putString("ACTION", action);
        ContactDetailsFragment fragment = new ContactDetailsFragment();

        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_details_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ContactDetailsViewModel.class);
        // TODO: Use the ViewModel
        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args.getLong(ARG_LONG_MY_NOTE_ID) != 0L) {
            ContactId = args.getLong(ARG_LONG_MY_NOTE_ID);
            action = args.getString("ACTION");

        }

        initView();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contact_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menu_save) {
            Name = FirstNamePlaceholder.getText().toString();
            ContactNumber = LastNamePlaceholder.getText().toString();
            Email = EmailPlaceholder.getText().toString();

            new MaterialDialog.Builder(getActivity())
                    .title(R.string.save_message_confirmation)
                    .negativeText(R.string.no_message)
                    .positiveText(R.string.yes_message)
                    .onPositive((dialog, which) -> {

                        HomePageViewModel = new ViewModelProvider(this, new HomePageViewModelFactory(getActivity().getApplication())).get(HomePageViewModel.class);
                        HomePageViewModel.editContact(ContactId, Name, ContactNumber, Email);
                        HomePageViewModel.loadMyContactsAdapterList();
                        Toast.makeText(getActivity(), "Contacts saved.", Toast.LENGTH_SHORT).show();

                        getActivity().setResult(ContactDetailsActivity.RESULT_CONTENT_MODIFIED);
                        getActivity().finish();
                    })
                    .show();
        }


        return super.onOptionsItemSelected(item);
    }


    public void initView() {
        ContactDetailsViewModel = new ViewModelProvider(this, new HomePageViewModelFactory(getActivity().getApplication())).get(HomePageViewModel.class);
        HomePageAdapter.Contacts dataholders = ContactDetailsViewModel.getContactDetails(ContactId);
        FirstNamePlaceholder.setText(dataholders.FirstName);
        LastNamePlaceholder.setText(dataholders.LastName);
        EmailPlaceholder.setText(dataholders.Email);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.bg_grey);

        Glide.with(getContext())
                .load(dataholders.Avatar)
                .apply(requestOptions)
                .into(ContactDetailsAvatar);


        if (action.equals("VIEW_ACTION")) {
            FirstNamePlaceholder.setEnabled(false);
            FirstNamePlaceholder.setBackgroundResource(android.R.color.transparent);
            FirstNamePlaceholder.setTypeface(null, Typeface.BOLD);
            FirstNamePlaceholder.setTextColor(Color.BLACK);

            LastNamePlaceholder.setEnabled(false);
            LastNamePlaceholder.setBackgroundResource(android.R.color.transparent);
            LastNamePlaceholder.setTypeface(null, Typeface.BOLD);
            LastNamePlaceholder.setTextColor(Color.BLACK);

            EmailPlaceholder.setEnabled(false);
            EmailPlaceholder.setBackgroundResource(android.R.color.transparent);
            EmailPlaceholder.setTypeface(null, Typeface.BOLD);
            EmailPlaceholder.setTextColor(Color.BLACK);


        } else if (action.equals("EDIT_ACTION")) {

            setHasOptionsMenu(true);

        }


    }


}
