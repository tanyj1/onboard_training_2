package com.example.onboardtraining2.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.onboardtraining2.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomePageAdapter extends RecyclerView.Adapter<HomePageAdapter.ViewHolder> {
    private Callbacks mCallbacks;
    private List<Contacts> mData = new ArrayList<>();
    private static Activity context;

    public HomePageAdapter(Activity activity) {
        context = activity;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<Contacts> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }


    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class Contacts {

        public Long id;
        public String FirstName;
        public String LastName;
        public String Email;
        public String Avatar;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public FrameLayout root;

        @BindView(R.id.FirstNameTextView)
        protected TextView FirstNameTextView;

        @BindView(R.id.ProfilePicture)
        protected ImageView ProfilePicture;

        @BindView(R.id.EmailTextView)
        protected TextView EmailTextView;

        @BindView(R.id.DeleteButton)
        protected Button DeleteButton;

        @BindView(R.id.EditButton)
        protected Button EditButton;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }


        public void bindTo(Contacts data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                String Full_Name = data.FirstName +" "+ data.LastName;
                if(Full_Name.length() > 14 ){
                    Full_Name = Full_Name.substring(0, 13);
                    Full_Name = Full_Name + "...";
                }
                FirstNameTextView.setText(Full_Name);
                EmailTextView.setText(data.Email);
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.bg_grey);

                Glide.with(context)
                        .load(data.Avatar)
                        .apply(requestOptions)
                        .into(ProfilePicture);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

                    DeleteButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onDeleteButtonClicked(data);
                        }
                    });

                    EditButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.OnEditButtonClicked(data);
                        }
                    });
                }
            }else{

            }
        }

        public void resetViews() {
            FirstNameTextView.setText("");

            EmailTextView.setText("");
            root.setOnClickListener(null);
            /*deleteButton.setOnClickListener(null);*/
        }
    }

    public interface Callbacks {
        void onListItemClicked(Contacts data);

        void onDeleteButtonClicked(Contacts data);

        void OnEditButtonClicked(Contacts data);


    }
}
