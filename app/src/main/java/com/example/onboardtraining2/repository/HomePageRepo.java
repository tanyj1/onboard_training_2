package com.example.onboardtraining2.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.onboardtraining2.adapter.HomePageAdapter;
import com.example.onboardtraining2.database.entities.Contact;
import com.example.onboardtraining2.database.entities.ContactDao;
import com.example.onboardtraining2.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class HomePageRepo {
    private static HomePageRepo HomePageInstance;
    private MutableLiveData<List<HomePageAdapter.Contacts>> ContactsAdapterListLiveData = new MutableLiveData<>();

    public static HomePageRepo getInstance() {
        if (HomePageInstance == null) {
            synchronized (HomePageRepo.class) {
                HomePageInstance = new HomePageRepo();
            }
        }

        return HomePageInstance;
    }

    private HomePageRepo() {

    }

    public LiveData<List<HomePageAdapter.Contacts>> getContactsAdapterListLiveData() {
        return ContactsAdapterListLiveData;
    }

    public void loadContactsAdapterList(Context context) {
        List<HomePageAdapter.Contacts> dataHolders = new ArrayList<>();
        List<Contact> records = DatabaseHelper.getInstance(context)
                .getContactDao()
                .loadAll();

        if (records != null) {
            for (Contact contactDetails : records) {
                HomePageAdapter.Contacts dataHolder = new HomePageAdapter.Contacts();
                dataHolder.id = contactDetails.getId();
                dataHolder.FirstName = contactDetails.getFirstName();
                dataHolder.LastName = contactDetails.getLastName();
                dataHolder.Email = contactDetails.getEmail();
                dataHolder.Avatar = contactDetails.getAvatar();

                dataHolders.add(dataHolder);
            }
        }

        ContactsAdapterListLiveData.setValue(dataHolders);
    }

    public void addContact(Context context, String FirstName, String LastName, String Email, String Avatar) {
        // Create new record.
        Contact contact = new Contact();
        contact.setEmail(Email);
        contact.setFirstName(FirstName);
        contact.setLastName(LastName);
        contact.setAvatar(Avatar);


        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getContactDao()
                .insert(contact);

        // Done adding record, now re-load list.
        loadContactsAdapterList(context);
    }

    public HomePageAdapter.Contacts getContactDetails(Context context, Long ContactId) {
        Contact dataHolders = new Contact();
        List<Contact> records = DatabaseHelper.getInstance(context)
                .getContactDao()
                .loadAll();
        HomePageAdapter.Contacts dataHolder = new HomePageAdapter.Contacts();
        if (records != null) {
            for (Contact contactDetails : records) {

                if (contactDetails.getId() == ContactId) {

                    dataHolder.id = contactDetails.getId();
                    dataHolder.FirstName = contactDetails.getFirstName();
                    dataHolder.LastName = contactDetails.getLastName();
                    dataHolder.Email = contactDetails.getEmail();
                    dataHolder.Avatar = contactDetails.getAvatar();


                    return dataHolder;
                }

            }
        }

        dataHolder = null;
        return dataHolder;
    }

    public void deleteContactDetails(Context context, Long ContactId) {
        // Delete record from database.
        DatabaseHelper.getInstance(context)
                .getContactDao()
                .deleteByKey(ContactId);

        // Done deleting record, now re-load list.
        loadContactsAdapterList(context);
    }

    public void editContactDetails(Context context, Long ContactId, String FirstName, String LastName, String Email) {
        ContactDao contactDao = DatabaseHelper.getInstance(context).getContactDao();
        Contact contact = contactDao.load(ContactId);

        // Check if record exists.
        if (contact != null) {
            // Record is found, now update.
            contact.setEmail(Email);
            contact.setFirstName(FirstName);
            contact.setLastName(LastName);

            contactDao.update(contact);

            // Done editing record, now re-load list.
            loadContactsAdapterList(context);
        }

    }

}
