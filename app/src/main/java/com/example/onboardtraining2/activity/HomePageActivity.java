package com.example.onboardtraining2.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.onboardtraining2.R;
import com.example.onboardtraining2.fragment.HomePageFragment;

import butterknife.ButterKnife;

public class HomePageActivity extends AppCompatActivity {
    public static final String TAG = HomePageActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_fragment_holder, HomePageFragment.newInstance(), HomePageActivity.TAG);
        fragmentTransaction.commit(); // save the changes
    }
}
