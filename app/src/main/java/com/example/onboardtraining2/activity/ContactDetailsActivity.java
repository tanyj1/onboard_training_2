package com.example.onboardtraining2.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.onboardtraining2.R;
import com.example.onboardtraining2.fragment.ContactDetailsFragment;

import butterknife.ButterKnife;

public class ContactDetailsActivity extends AppCompatActivity {


    public static final String TAG = ContactDetailsActivity.class.getSimpleName();
    public static final String EXTRA_LONG_MY_CONTACT_ID = "EXTRA_LONG_MY_NOTE_ID";

    public static final int RESULT_CONTENT_MODIFIED = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Intent intent = getIntent();
        long ContactId = 0;
        Bundle extras = intent.getExtras();

        String action = "VIEW_ACTION";

        if (extras.getLong(EXTRA_LONG_MY_CONTACT_ID) != 0L) {
            ContactId = extras.getLong(EXTRA_LONG_MY_CONTACT_ID);
            if (extras.getString("ACTION").equals("EDIT_ACTION")) {
                action = "EDIT_ACTION";
            }

        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_fragment_holder, ContactDetailsFragment.newInstance(ContactId, action), ContactDetailsActivity.TAG);
        fragmentTransaction.commit(); // save the changes

    }
}
