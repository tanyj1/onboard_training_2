package com.example.onboardtraining2.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.onboardtraining2.R;
import com.example.onboardtraining2.fragment.TermFragment;

import butterknife.ButterKnife;

public class TermsActivity extends AppCompatActivity {
    public static final String TAG = TermsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_fragment_holder, TermFragment.newInstance(), TermsActivity.TAG);
        fragmentTransaction.commit(); // save the changes
    }
}
